import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';

class JobApplications extends StatefulWidget {
  const JobApplications({Key key}) : super(key: key);

  @override
  _JobApplicationsState createState() => _JobApplicationsState();
}

class _JobApplicationsState extends State<JobApplications> {
  String _type;
  List list = [
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
    'Ramesh Sharma',
  ];
  var minDisplay;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    minDisplay = 10 < list.length ? 10 : list.length;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RoundedAppbar(
        appbarColor: Colours.orange,
        backgroundColor: Colours.pink,
        bottomBGColor: Colours.pink,
        title: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            'Application',
            style: HeaderW2,
          ),
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Applicants(${list.length})',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          Container(
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            child: Column(
              children: [
                for (var i = 0; i < minDisplay; i++)
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "${list[i]}",
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          'View',
                          style: TextStyle(
                              color: Colours.blue,
                              decoration: TextDecoration.underline),
                        ),
                      ],
                    ),
                  ),
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colours.grey,
                  blurRadius: 5,
                  offset: const Offset(5, 5),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
