import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';

class NewJob extends StatefulWidget {
  const NewJob({Key key}) : super(key: key);

  @override
  _NewJobState createState() => _NewJobState();
}

class _NewJobState extends State<NewJob> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RoundedAppbar(
            appbarColor: Colours.orange,
            backgroundColor: Colours.pink,
            bottomBGColor: Colours.pink,
            title: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              title: Text(
                'Enter Job Details',
                style: HeaderW2,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Label('Job Title'),
                Input(
                  child: TextFormField(
                    decoration:
                        InputStyle(label: '').input(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Label('Job Role'),
                Input(
                  child: TextFormField(
                    minLines: 4,
                    maxLines: 6,
                    decoration:
                        InputStyle(label: '').input(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Label('Job Qualification'),
                Input(
                  child: TextFormField(
                    minLines: 4,
                    maxLines: 6,
                    decoration:
                        InputStyle(label: '').input(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Label('Pay Range'),
                Input(
                  child: TextFormField(
                    decoration: InputStyle(label: 'Rs. 8-10 LPA(Negotiable)')
                        .input(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Label('Process of Selection'),
                Input(
                  child: TextFormField(
                    minLines: 4,
                    maxLines: 6,
                    decoration:
                        InputStyle(label: '').input(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
              ],
            ),
            submitBtn: SubmitButton(
              text: 'Post',
              onSubmit: () {
                Navigator.pop(context);
              },
            )));
  }
}
