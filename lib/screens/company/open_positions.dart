import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:skillbook/widgets/jobCard.dart';

class OpenPositions extends StatefulWidget {
  const OpenPositions({Key key}) : super(key: key);

  @override
  _OpenPositionsState createState() => _OpenPositionsState();
}

class _OpenPositionsState extends State<OpenPositions> {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(fontFamily: 'BebasNeue'),
        child: Scaffold(
            body: RoundedAppbar(
          appbarColor: Colours.orange,
          backgroundColor: Colours.pink,
          bottomBGColor: Colours.pink,
          title: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: Icon(Icons.menu),
            title: Text(
              'HOME',
            ),
            actions: [
              IconButton(
                icon: Icon(Icons.chat_bubble_outline, color: Colors.black),
                onPressed: () {},
              ),
              CircleAvatar(
                radius: 15,
              )
            ],
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Open Positions',
                  style: HeaderB2,
                ),
                IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      Navigator.pushNamed(context, new_job);
                    })
              ],
            ),
            JobCard(
              jobtitle: 'SDE - I',
              company: 'Skillbook',
              location: 'Pune, Maharashtra',
              ctc: '8-10 LPA',
              views: 250,
            ),
            JobCard(
              jobtitle: 'SDE - II',
              company: 'Skillbook',
              location: 'Pune, Maharashtra',
              ctc: '8-10 LPA',
              views: 250,
            )
          ]),
        )));
  }
}
