import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skillbook/screens/onboarding.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isOnboarding = false;

  @override
  initState() {
    super.initState();
    Timer(const Duration(seconds: 5), onNavigate);
  }

  void onNavigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isOnboarding =
        // prefs.getBool("isonboarding") ??
        true;
    if (isOnboarding) {
      Navigator.of(context).pushReplacement(new PageRouteBuilder(
        maintainState: true,
        opaque: true,
        pageBuilder: (context, _, __) => new OnBoarding(),
      ));
    } else {
      Navigator.of(context).pushReplacementNamed(account_details);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colours.pink,
          body: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'SMITKIRAN PUBLISHING ',
                    style: TextStyle(fontSize: 18, color: Colours.orange),
                  ),
                  Text(
                    'PRIVATE LIMITED',
                    style: TextStyle(fontSize: 18, color: Colours.green),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  'assets/images/skillbook_logo.png',
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'CONNECTING TRAINEES ',
                    style: TextStyle(fontSize: 18, color: Colours.orange),
                  ),
                  Text(
                    'TO SKILLS',
                    style: TextStyle(fontSize: 18, color: Colours.green),
                  )
                ],
              ),
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                    color: Colours.orange,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 2,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 10),
                          Text(
                            'CONNECTING',
                            style: HeaderW1,
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              height: 2,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'One Crore Trainees\nto\nOne Lakh Skills',
                      textAlign: TextAlign.center,
                      style: HeaderW2,
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 50,
                          width: 80,
                          alignment: Alignment.center,
                          child: Text(
                            '1 LAKH\nSKILLS',
                            style: HeaderW3,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.white),
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        SizedBox(width: 10),
                        Container(
                          height: 50,
                          width: 80,
                          alignment: Alignment.center,
                          child: Text(
                            '1684\nUSERS',
                            style: HeaderW3,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.white),
                            borderRadius: BorderRadius.circular(5),
                          ),
                        )
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: IconButton(
                        icon: Icon(
                          FontAwesome5Solid.angle_double_down,
                          color: Colors.white,
                          size: 30,
                        ),
                        onPressed: () {
                          onNavigate();
                        },
                      ),
                    )
                  ],
                ),
              ))
            ],
          )),
    );
  }
}
