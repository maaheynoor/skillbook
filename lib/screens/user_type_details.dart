import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';

class UserTypeDetails extends StatefulWidget {
  UserTypeDetails({this.type});
  final String type;

  @override
  _UserTypeDetailsState createState() => _UserTypeDetailsState();
}

class _UserTypeDetailsState extends State<UserTypeDetails> {
  Map<String, List> accountTypeList = {
    //image name, widget, Navigate to
    'Trainee': ['trainee', TraineeForm(), student_dashboard],
    'Crafts Instructor': [
      'craft_instructor',
      CraftInstForm(),
      craft_inst_courses
    ],
    'ITI': ['iti', ITIForm(), inst_teachers],
    'BTRI': ['btri', BTRIForm(), inst_teachers],
    'Company/Industry': ['company-industry', CompanyForm(), open_positions],
    'Skill Man': ['skillman', SkillManForm(), skillman_services],
    'School/College/Institute': [
      'school-college-institute',
      InstituteForm(),
      inst_teachers
    ],
    'Teacher/Professor': ['teacher-professor', TeacherForm(), inst_teachers],
    'Student': ['student', StudentForm(), reels]
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/images/cont_${accountTypeList[widget.type][0]}.png',
                  fit: BoxFit.fitWidth,
                  width: double.infinity,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: accountTypeList[widget.type][1]),
                SizedBox(height: 60),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'Account will be verified after approval from admin',
                    style: TextStyle(color: Colours.grey),
                  ),
                ),
                SizedBox(height: 60),
              ],
            ),
          ),
          SubmitButton(onSubmit: () {
            Navigator.pushNamed(context, accountTypeList[widget.type][2]);
          })
        ],
      ),
    ));
  }
}

class TraineeForm extends StatefulWidget {
  const TraineeForm({Key key}) : super(key: key);

  @override
  _TraineeFormState createState() => _TraineeFormState();
}

class _TraineeFormState extends State<TraineeForm> {
  List<String> tradeList = ['Domestic', 'International'];
  List<String> trainingList = ['1 month', '3 months', '6 months', '1 year'];
  String trade, trainingp;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('Trade'),
        PinkDropdown(
          list: tradeList,
          value: trade,
          onChanged: (val) {
            setState(() {
              trade = val;
            });
          },
        ),
        SizedBox(
          height: 10,
        ),
        Label('Training Period'),
        PinkDropdown(
          list: trainingList,
          value: trainingp,
          onChanged: (val) {
            setState(() {
              trainingp = val;
            });
          },
        ),
        SizedBox(
          height: 10,
        ),
        Label('Trainee ITI Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('ITI PIN Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 60,
        ),
      ],
    );
  }
}

class CraftInstForm extends StatefulWidget {
  const CraftInstForm({Key key}) : super(key: key);

  @override
  _CraftInstFormState createState() => _CraftInstFormState();
}

class _CraftInstFormState extends State<CraftInstForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('ITI Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('ITI NCVT Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        // Label('Upload ID Photo'),
        // GestureDetector(
        //   onTap: () {},
        //   child: AbsorbPointer(
        //     child: Input(
        //       child: TextFormField(
        //         decoration:
        //             InputStyle(label: 'Browse').input(color: Colours.pink),
        //       ),
        //     ),
        //   ),
        // ),
      ],
    );
  }
}

class ITIForm extends StatefulWidget {
  const ITIForm({Key key}) : super(key: key);

  @override
  _ITIFormState createState() => _ITIFormState();
}

class _ITIFormState extends State<ITIForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('ITI Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('ITI Address'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('ITI NCVT Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principal Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principle Phone Number'),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 4,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload ID Photo'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class BTRIForm extends StatefulWidget {
  const BTRIForm({Key key}) : super(key: key);

  @override
  _BTRIFormState createState() => _BTRIFormState();
}

class _BTRIFormState extends State<BTRIForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('BTRI Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('BTRI Address'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('BTRI NCVT Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principal Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principle Phone Number'),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 4,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload ID Photo'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class CompanyForm extends StatefulWidget {
  const CompanyForm({Key key}) : super(key: key);

  @override
  _CompanyFormState createState() => _CompanyFormState();
}

class _CompanyFormState extends State<CompanyForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('Industry/Shop Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        // Label('Affiliation'),
        // Input(
        //   child: TextFormField(
        //     decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
        //   ),
        // ),
        SizedBox(
          height: 10,
        ),
        Label('Industry/Shop License No.'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Owner Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Owner Phone Number'),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 4,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload ID Photo'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class SkillManForm extends StatefulWidget {
  const SkillManForm({Key key}) : super(key: key);

  @override
  _SkillManFormState createState() => _SkillManFormState();
}

class _SkillManFormState extends State<SkillManForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('Trade'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Seat No.'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Year of Passing'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload Marks'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class InstituteForm extends StatefulWidget {
  const InstituteForm({Key key}) : super(key: key);

  @override
  _InstituteFormState createState() => _InstituteFormState();
}

class _InstituteFormState extends State<InstituteForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('School/College/Institute Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('School/College/Institute Address'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('School/College/Institute Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principal/Dean/Owner Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Principal/Dean/Owner Phone Number'),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 4,
              child: Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colours.pink),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload ID Photo'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class TeacherForm extends StatefulWidget {
  const TeacherForm({Key key}) : super(key: key);

  @override
  _TeacherFormState createState() => _TeacherFormState();
}

class _TeacherFormState extends State<TeacherForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('Teacher/Professor Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('School/College/Institute Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Upload School/College/Institute ID'),
        GestureDetector(
          onTap: () {},
          child: AbsorbPointer(
            child: Input(
              child: TextFormField(
                decoration:
                    InputStyle(label: 'Browse').input(color: Colours.pink),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class StudentForm extends StatefulWidget {
  const StudentForm({Key key}) : super(key: key);

  @override
  _StudentFormState createState() => _StudentFormState();
}

class _StudentFormState extends State<StudentForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('School Name'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('Standard'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Label('School PIN Code'),
        Input(
          child: TextFormField(
            decoration: InputStyle(label: 'Enter').input(color: Colours.pink),
          ),
        ),
      ],
    );
  }
}
