import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:intl/intl.dart';
import 'package:country_picker/country_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewAccount extends StatefulWidget {
  const NewAccount({Key key}) : super(key: key);

  @override
  _NewAccountState createState() => _NewAccountState();
}

class _NewAccountState extends State<NewAccount> {
  List<String> genderList = ['Male', 'Female', 'Transgender'];
  List<String> accountTypeList = [
    'Trainee',
    'Crafts Instructor',
    'ITI',
    'BTRI',
    'Company/Industry',
    'Skill Man',
    'School/College/Institute',
    'Teacher/Professor',
    'Student'
  ];

  String gender, accountType;
  bool error = false;
  DateTime _selectedDate = DateTime.now();
  var _datecontroller = TextEditingController();
  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(1600),
        lastDate: DateTime.now(),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.light(
                  primary: Colours.pink,
                  onPrimary: Colors.white,
                  surface: Colours.pink,
                  onSurface: Colors.black),
              dialogBackgroundColor: Colours.pink,
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        _selectedDate = picked;
        _datecontroller.text = DateFormat('dd/MM/yyyy').format(picked);
      });
  }

  _showCountryPicker() {
    showCountryPicker(
      context: context,
      onSelect: (Country country) {
        print('Select country: ${country.displayName}');
      },
      countryListTheme: CountryListThemeData(backgroundColor: Colours.pink),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/images/new_account_details.PNG',
                  fit: BoxFit.fitWidth,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Label('Enter Name'),
                      Input(
                        child: TextFormField(
                          decoration: InputStyle(label: 'First Name')
                              .input(color: Colours.pink),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Input(
                        child: TextFormField(
                          decoration: InputStyle(label: 'Middle Name')
                              .input(color: Colours.pink),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Input(
                        child: TextFormField(
                          decoration: InputStyle(label: 'Last Name')
                              .input(color: Colours.pink),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Nationality'),
                      GestureDetector(
                        onTap: _showCountryPicker,
                        child: AbsorbPointer(
                          child: Input(
                            child: TextFormField(
                              controller: _datecontroller,
                              decoration: InputStyle(date: false).datepicker(),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Gender'),
                      PinkDropdown(
                        list: genderList,
                        value: gender,
                        onChanged: (val) {
                          setState(() {
                            gender = val;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Date of Birth'),
                      GestureDetector(
                        onTap: _selectDate,
                        child: AbsorbPointer(
                          child: Input(
                            child: TextFormField(
                              controller: _datecontroller,
                              decoration: InputStyle(date: true).datepicker(),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Account Type'),
                      PinkDropdown(
                        list: accountTypeList,
                        value: accountType,
                        onChanged: (val) {
                          setState(() {
                            accountType = val;
                          });
                        },
                      ),
                      error && accountType == null
                          ? Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: Text(
                                'Account type is required',
                                style: TextStyle(color: Colours.grey),
                              ),
                            )
                          : Container(),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Mobile Number'),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Input(
                              child: TextFormField(
                                decoration: InputStyle(label: '')
                                    .input(color: Colours.pink),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            flex: 4,
                            child: Input(
                              child: TextFormField(
                                decoration: InputStyle(label: '')
                                    .input(color: Colours.pink),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Label('Address'),
                      Input(
                        child: TextFormField(
                          decoration: InputStyle(label: 'Address Line 1')
                              .input(color: Colours.pink),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Input(
                        child: TextFormField(
                          decoration: InputStyle(label: 'Address Line 2')
                              .input(color: Colours.pink),
                        ),
                      ),
                      SizedBox(
                        height: 60,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SubmitButton(onSubmit: () async {
            if (accountType != null) {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString("account_type", accountType);
              Navigator.pushNamed(context, account_type_details,
                  arguments: accountType);
            } else {
              setState(() {
                error = true;
              });
            }
          })
        ],
      ),
    ));
    ;
  }
}
