import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class Settings extends StatefulWidget {
  const Settings({Key key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  var settingsList = {
    'Name': 'Aditya Sharma',
    'Email': 'aditya@gmail.com',
    'Password': 'Updated 2 days ago',
    'Phone Number': '+91 323xxxxx'
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: [
              Icon(Icons.arrow_back, color: Colors.black),
              Text('BACK', style: TextStyle(color: Colors.black))
            ],
          ),
        ),
        leadingWidth: double.maxFinite,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              'SETTINGS',
              style: HeaderB2,
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  for (var s in settingsList.keys)
                    ListTile(
                      leading: SizedBox(width: 10),
                      minLeadingWidth: 20.0,
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            s,
                            style: TextStyle(color: Colours.grey),
                          ),
                          Text(
                            settingsList[s],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      trailing: Icon(
                        Icons.chevron_right,
                        color: Colours.grey,
                      ),
                    ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  ListTile(
                    minLeadingWidth: 20.0,
                    leading: Icon(
                      Icons.help_outline,
                      color: Colors.grey,
                      size: 20,
                    ),
                    title: Label('Help Center'),
                  ),
                  ListTile(
                    minLeadingWidth: 20.0,
                    leading: SizedBox(width: 10),
                    title: Label('Privacy Terms'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
