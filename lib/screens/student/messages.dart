import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/utils/text_styles.dart';

class Messages extends StatefulWidget {
  const Messages({Key key}) : super(key: key);

  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  int active = 1;
  List msgTypes = ['Groups', 'Chats'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: EdgeInsets.only(bottom: 30),
                  width: double.infinity,
                  color: Colours.green,
                  child: AppBar(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    title: Text(
                      'MESSAGES',
                      style: HeaderW2,
                    ),
                    actions: [
                      IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(FontAwesome.ellipsis_v),
                        onPressed: () {},
                      ),
                    ],
                  )),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (active == 0)
                      active = 1;
                    else
                      active = 0;
                  });
                },
                child: Transform.translate(
                  offset: Offset(0.0, -20),
                  child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(20))),
                      child: Text(
                        active == 0 ? msgTypes[1] : msgTypes[0],
                        style: HeaderG3,
                      )),
                ),
              ),
              Transform.translate(
                offset: Offset(0.0, -20),
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          msgTypes[active],
                          style: HeaderW3,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        CircleAvatar(
                          radius: 10,
                          backgroundColor: Colors.white,
                          child: Text(
                            '2',
                            style: HeaderG3,
                          ),
                        )
                      ],
                    )),
              ),
              for (var i = 0; i < 3; i++)
                active == 1
                    ? ChatTile(
                        title: 'A Ghosh',
                        message: 'Thanks for joining the course!',
                        time: '10:01')
                    : ChatTile(
                        title: 'Programming Group',
                        message: 'Thanks for joining the course!',
                        time: '10:05')
            ],
          ),
        ),
      ),
    );
  }
}

class ChatTile extends StatelessWidget {
  const ChatTile({this.title, this.message, this.time});
  final title, message, time;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colours.grey,
            offset: Offset(0, 2),
            blurRadius: 5,
            spreadRadius: 0)
      ]),
      child: ListTile(
        leading: CircleAvatar(
          radius: 20,
          backgroundColor: Colours.grey,
        ),
        title: Text(title),
        subtitle: Text(message),
        trailing:
            Text(time, style: TextStyle(color: Colours.green, fontSize: 10)),
      ),
    );
  }
}
