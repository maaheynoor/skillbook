import 'package:flutter/material.dart';
import 'package:skillbook/screens/craft_instructor/all_courses.dart';

class MyCourses extends StatefulWidget {
  const MyCourses({Key key}) : super(key: key);

  @override
  _MyCoursesState createState() => _MyCoursesState();
}

class _MyCoursesState extends State<MyCourses> {
  @override
  Widget build(BuildContext context) {
    return AllCoursesPage();
  }
}
