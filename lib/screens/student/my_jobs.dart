import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/jobCard.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/customTabs.dart';

class MyJobs extends StatefulWidget {
  const MyJobs({Key key}) : super(key: key);

  @override
  _MyJobsState createState() => _MyJobsState();
}

class _MyJobsState extends State<MyJobs> {
  var activeTab = 0;
  List tabs = [
    ['Applied', 5],
    ['Shortlisted', 2],
    ['Offers', 1]
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: TextFormField(
          decoration: InputStyle(label: 'Search').search(),
        ),
        actions: [
          CircleAvatar(
            radius: 15,
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                for (var i = 0; i < tabs.length; i++)
                  CustomTab(
                    title: tabs[i][0],
                    number: tabs[i][1],
                    active: i == activeTab,
                    onTap: () {
                      setState(() {
                        activeTab = i;
                      });
                    },
                  )
              ],
            ),
            for (var i = 0; i < tabs[activeTab][1]; i++)
              WhiteJobCard(
                image: 'assets/images/python.png',
                role: 'Python Intern',
                company: 'Birla Industries',
                jobtype: 'Work from Home/ Remote',
                applicants: 56,
                postdate: 5,
              )
          ],
        ),
      ),
    );
  }
}
