import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/jobCard.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/customTabs.dart';
import 'package:skillbook/widgets/form_fields.dart';

class SkillDevelop extends StatefulWidget {
  const SkillDevelop({Key key}) : super(key: key);

  @override
  _SkillDevelopState createState() => _SkillDevelopState();
}

class _SkillDevelopState extends State<SkillDevelop> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Skill Development & Entrepreneurship',
                      style: HeaderB1,
                    ),
                    Label('What groups are you looking for?'),
                    Input(
                      child: TextFormField(
                        decoration: InputStyle(label: 'Enter')
                            .input(color: Colors.white),
                      ),
                    ),
                    Label('Interested in'),
                    Input(
                      child: TextFormField(
                        decoration: InputStyle(label: 'Enter')
                            .input(color: Colors.white),
                      ),
                    ),
                    Label('Qualifications'),
                    Input(
                      child: TextFormField(
                        decoration: InputStyle(label: 'Enter')
                            .input(color: Colors.white),
                      ),
                    ),
                  ],
                )),
            SubmitButton(
                text: 'SHOW INTEREST',
                onSubmit: () {
                  Navigator.pushNamed(context, skill_groups);
                })
          ],
        ),
      ),
    );
  }
}
