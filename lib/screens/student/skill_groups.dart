import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/jobCard.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/customTabs.dart';
import 'package:skillbook/widgets/form_fields.dart';

class SkillGroups extends StatefulWidget {
  const SkillGroups({Key key}) : super(key: key);

  @override
  _SkillGroupsState createState() => _SkillGroupsState();
}

class _SkillGroupsState extends State<SkillGroups> {
  var location, group, investment;
  var location_list = ['X', 'XI', 'XII', 'Under Grad Under Grad', 'Grad'];
  var group_list = ['Btech', 'B.E.', 'B.A.', 'B.Sc.'];
  var investment_list = ['Vidyalay', 'School', 'COE'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: TextFormField(
          decoration: InputStyle(label: 'Search').search(),
        ),
        actions: [
          CircleAvatar(
            radius: 15,
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              'Groups you may be interested in',
              style: HeaderB1,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  WhiteDropdown(
                      list: location_list,
                      value: location,
                      onChanged: (val) {
                        setState(() {
                          location = val;
                        });
                      },
                      hint: 'Location'),
                  SizedBox(width: 10),
                  WhiteDropdown(
                      list: group_list,
                      value: group,
                      onChanged: (val) {
                        setState(() {
                          group = val;
                        });
                      },
                      hint: 'Group'),
                  SizedBox(width: 10),
                  WhiteDropdown(
                      list: investment_list,
                      value: investment,
                      onChanged: (val) {
                        setState(() {
                          investment = val;
                        });
                      },
                      hint: 'Investment'),
                ],
              ),
            ),
            SizedBox(height: 10),
            WhiteJobCard(
              image: 'assets/images/python.png',
              role: 'Python Intern',
              company: 'Birla Industries',
              jobtype: 'Work from Home/ Remote',
              applicants: 56,
              postdate: 5,
            )
          ],
        ),
      ),
    );
  }
}
