import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class UpgradeAccount extends StatefulWidget {
  const UpgradeAccount({Key key}) : super(key: key);

  @override
  _UpgradeAccountState createState() => _UpgradeAccountState();
}

class _UpgradeAccountState extends State<UpgradeAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(Icons.arrow_back, color: Colors.black),
                    Text('BACK', style: TextStyle(color: Colors.black))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Upgrade Account',
                      style: HeaderB1,
                    ),
                    SizedBox(height: 10),
                    PlanCard(
                      plan: 'Silver Plan',
                      price: 500,
                      details: [
                        'Verified Account',
                        'See company details for jobs and apprentice'
                      ],
                      color: Colors.grey,
                    ),
                    SizedBox(height: 20),
                    PlanCard(
                        plan: 'Gold Plan',
                        price: 500,
                        details: [
                          'Verified Account',
                          'See company details for jobs and apprentice',
                          'Message Alert on Phone and Email'
                        ],
                        color: Colors.amber[200]),
                    SizedBox(height: 20),
                    PlanCard(
                        plan: 'Diamond Plan',
                        price: 10000,
                        details: [
                          'Verified Account',
                          'See company details for jobs and apprentice',
                          'Message Alert on Phone and Email',
                          'Bio-data sent to company'
                        ],
                        color: Colors.blue[100]),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PlanCard extends StatelessWidget {
  const PlanCard(
      {this.plan, this.price, this.details, this.onSelect, this.color});
  final plan, price, details, onSelect, color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.1, 0.9],
            colors: [
              color,
              color.withOpacity(0.01),
            ],
          ),
          border: Border.all(color: Colors.black)),
      child: Column(
        children: [
          Text(
            plan,
            style: HeaderB1,
          ),
          Text.rich(TextSpan(children: [
            WidgetSpan(child: Icon(FontAwesome.inr)),
            TextSpan(text: ' ${price} per month'),
          ])),
          for (String d in details)
            ListTile(
                leading: Icon(
                  Icons.check,
                  color: Colours.darkgreen,
                ),
                title: Text(d)),
          Center(
            child: OutlinedButton(
              onPressed: () {},
              child: Text(
                'UPGRADE',
                style: TextStyle(color: Colors.black),
              ),
              style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  side: BorderSide(color: Colors.black),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
          ),
        ],
      ),
    );
  }
}
