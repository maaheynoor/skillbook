import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.orange,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Notifications',
              style: HeaderW2,
            ),
            Align(
              alignment: Alignment.topRight,
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 10,
                child: Text(
                  '2',
                  style:
                      TextStyle(color: Colours.blue, fontFamily: 'BebasNeue'),
                ),
              ),
            )
          ],
        ),
        actions: [Icon(Icons.search), Icon(FontAwesome.ellipsis_v)],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Notif(
              title: 'ACCOUNT VERIFICATION',
              time: '10:08',
              desc: 'Submit your documents to be verified',
            ),
            Notif(
              title: 'ASSIGNMENT DUE TODAY',
              time: '10:08',
              desc: 'Submit your thermodynamic assignment',
            )
          ],
        ),
      ),
    );
  }
}

class Notif extends StatelessWidget {
  const Notif({this.title, this.time, this.desc});
  final title, time, desc;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      color: Colours.pink,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(fontSize: 16),
              ),
              Text(time, style: TextStyle(color: Colours.blue))
            ],
          ),
          Text(desc, style: TextStyle(color: Colors.black38))
        ],
      ),
    );
  }
}
