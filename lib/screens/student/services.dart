import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class ServicesPage extends StatefulWidget {
  const ServicesPage({Key key}) : super(key: key);

  @override
  _ServicesPageState createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  List services = [
    'Carpenter',
    'Hair & Skin Care',
    'Maid',
    'Painter',
    'Electrician',
    'Plumber',
    'Doctor',
    'Tailer',
    'Chef'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Services',
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colours.grey,
                      hintText: 'Search here',
                      suffixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colours.grey,
                      hintText: '401202',
                      suffixIcon: Icon(Icons.location_on),
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 20),
            Text(
              'Select the services you want',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 10),
            for (var i = 0; i < services.length / 3; i++)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  for (var j = 0; j < 3; j++)
                    (i * 3 + j < services.length)
                        ? ServiceCard(
                            img: 'assets/images/user_icon.PNG',
                            title: services[i * 3 + j],
                            onTap: () {
                              Navigator.pushNamed(context, searchskillman);
                            },
                          )
                        : Container(
                            padding: EdgeInsets.all(10),
                            height: MediaQuery.of(context).size.width / 4,
                            width: MediaQuery.of(context).size.width / 4,
                          ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}

class ServiceCard extends StatelessWidget {
  ServiceCard({this.img, this.title, this.onTap});
  final img, title, onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
            child: ClipOval(
              child: Image.asset(
                img,
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.width / 4,
                width: MediaQuery.of(context).size.width / 4,
              ),
            ),
            onTap: onTap),
        Text(
          title,
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }
}
