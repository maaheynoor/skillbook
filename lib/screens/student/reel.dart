import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/routes.dart';

class ReelPage extends StatefulWidget {
  const ReelPage({Key key}) : super(key: key);

  @override
  _ReelPageState createState() => _ReelPageState();
}

class _ReelPageState extends State<ReelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(children: [
          Image.asset(
            'assets/images/reel1.png',
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.fill,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pushReplacementNamed(
                            context, student_dashboard);
                      }),
                  Icon(
                    Icons.menu,
                    color: Colors.white,
                  )
                ],
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 150,
                      ),
                      Icon(
                        Icons.circle,
                        color: Colors.white,
                      ),
                      Icon(
                        FontAwesome5.heart,
                        color: Colors.white,
                      ),
                      Icon(
                        Icons.chat,
                        color: Colors.white,
                      ),
                      Icon(
                        Icons.mobile_screen_share,
                        color: Colors.white,
                      ),
                      Icon(
                        Icons.list_alt,
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pushReplacementNamed(
                            context, student_dashboard);
                      }),
                  Icon(
                    Icons.upload_file,
                    color: Colors.white,
                  ),
                  IconButton(
                    icon: Icon(
                      FontAwesome5Solid.angle_double_down,
                      color: Colors.white,
                    ),
                    onPressed: () => Navigator.pushReplacement(
                        context, SlideBottomRoute(page: ReelPage())),
                  ),
                  Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  Icon(
                    Icons.person_rounded,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          )
        ]),
      ),
    );
  }
}

class SlideBottomRoute extends PageRouteBuilder {
  final Widget page;
  SlideBottomRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, -1),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        );
}
