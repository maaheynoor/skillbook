import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class SearchSkillMan extends StatefulWidget {
  const SearchSkillMan({Key key}) : super(key: key);

  @override
  _SearchSkillManState createState() => _SearchSkillManState();
}

class _SearchSkillManState extends State<SearchSkillMan> {
  @override
  bool isLoading = true;
  List skillman = [
    {
      'name': 'Alden Cantrell',
      'star': 4,
      'address': 'Bhiwandi Road, Thane (west) 400181',
      'mobile': '912122121'
    },
    {
      'name': 'Piere Cox',
      'star': 5,
      'address': 'Bhiwandi Road, Thane (west) 400181',
      'mobile': '912122121'
    }
  ];

  @override
  initState() {
    super.initState();
    Timer(const Duration(seconds: 1), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  Widget build(BuildContext context) {
    return isLoading
        ? Scaffold(
            backgroundColor: Colours.lightpink,
            body: Column(
              children: [
                Container(
                  height: 50,
                  color: Colours.pink,
                ),
                Image.asset(
                  'assets/images/search_skillman.PNG',
                  fit: BoxFit.contain,
                  width: MediaQuery.of(context).size.width,
                ),
              ],
            ))
        : Scaffold(
            backgroundColor: Colors.grey[300],
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              title: Text(
                'Services',
                style: TextStyle(color: Colors.black),
              ),
              iconTheme: IconThemeData(color: Colors.black),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colours.grey,
                      hintText: 'Search here',
                      suffixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Select the services you want',
                        style: TextStyle(fontSize: 18),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: Row(
                          children: [
                            Icon(FontAwesome.filter, color: Colors.black54),
                            Text(
                              'Filter',
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colours.grey,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  for (var i = 0; i < skillman.length; i++)
                    CollapsibleCard(
                      skillman: skillman[i],
                    )
                ],
              ),
            ),
          );
  }
}

class StarRating extends StatelessWidget {
  StarRating(this.rating);
  final rating;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        for (var j = 0; j < rating; j++)
          Icon(
            Icons.star,
            color: Colours.green,
            size: 16,
          ),
        for (var j = rating; j < 5; j++)
          Icon(
            Icons.star_outline,
            color: Colours.green,
            size: 16,
          ),
      ],
    );
  }
}

class CollapsibleCard extends StatefulWidget {
  const CollapsibleCard({this.skillman});
  final skillman;

  @override
  _CollapsibleCardState createState() => _CollapsibleCardState();
}

class _CollapsibleCardState extends State<CollapsibleCard> {
  bool open = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.skillman['name']),
                StarRating(widget.skillman['star']),
              ],
            ),
            onTap: () {
              setState(() {
                open = !open;
              });
            },
            leading: ClipOval(
              child: Image.asset(
                'assets/images/user.PNG',
                width: 50,
                height: 50,
              ),
            ),
            trailing:
                open ? Icon(Icons.arrow_downward) : Icon(Icons.arrow_forward),
          ),
          open
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.skillman['address']),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Mobile - ${widget.skillman["mobile"]}'),
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              'Call',
                              style: TextStyle(color: Colors.black),
                            ),
                            style: TextButton.styleFrom(
                                backgroundColor: Colours.green,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20))),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
