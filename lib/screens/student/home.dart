import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/stackedScreen.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return StackedScreen(
        mainWidgets: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                'Continue',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
            CourseCard(
                coursename: 'Python Programming',
                instructor: 'Andrew Ng',
                enroll_date: '11/05/21',
                completed_percent: 80,
                image: 'assets/images/python.png'),
            CourseCard(
                coursename: 'Python Programming',
                instructor: 'Andrew Ng',
                enroll_date: '11/05/21',
                completed_percent: 80,
                image: 'assets/images/python.png'),
            CourseCard(
                coursename: 'Python Programming',
                instructor: 'Andrew Ng',
                enroll_date: '11/05/21',
                completed_percent: 30,
                image: 'assets/images/python.png'),
          ],
        ),
        onViewMore: () {
          Navigator.pushNamed(context, all_courses, arguments: true);
        },
        bottomWidgets:
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              'Browse',
              style: HeaderB2,
            ),
          ),
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                ],
              )),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              'Top Courses',
              style: HeaderB2,
            ),
          ),
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  CourseCard(
                    coursename: 'Python Programming',
                    instructor: 'Andrew Ng',
                    videos: 22,
                    students: 230,
                    image: 'assets/images/python.png',
                    scroll: true,
                  ),
                ],
              )),
        ]));
  }
}
