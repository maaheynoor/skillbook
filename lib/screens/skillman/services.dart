import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:skillbook/screens/student/services.dart';

class Services extends StatefulWidget {
  const Services({Key key}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  String _type;
  List services = [
    'Carpenter',
    'Hair & Skin Care',
    'Maid',
    'Painter',
    'Electrician',
    'Plumber',
    'Doctor',
    'Tailer',
    'Chef'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colours.pink,
        appBar: AppBar(
            backgroundColor: Colours.orange,
            elevation: 0,
            toolbarHeight: 120,
            automaticallyImplyLeading: false,
            title: Column(
              children: [
                Text.rich(TextSpan(children: [
                  WidgetSpan(child: Icon(Icons.location_on)),
                  TextSpan(text: 'Rove Land, 29 A/B Wing, Andheri, Mumbai...')
                ])),
                TextFormField(
                  decoration: InputStyle(label: 'Search').search(),
                ),
              ],
            )),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                for (var i = 0; i < services.length / 3; i++)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      for (var j = 0; j < 3; j++)
                        (i * 3 + j < services.length)
                            ? ServiceCard(
                                img: 'assets/images/user_icon.PNG',
                                title: services[i * 3 + j],
                                onTap: () {
                                  Navigator.pushNamed(context, searchskillman);
                                },
                              )
                            : Container(
                                padding: EdgeInsets.all(10),
                                height: MediaQuery.of(context).size.width / 4,
                                width: MediaQuery.of(context).size.width / 4,
                              ),
                    ],
                  ),
                SizedBox(height: 20),
                Text(
                  'Top Rated Services in your area',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                ServiceDetailCard(),
                ServiceDetailCard(),
              ],
            ),
          ),
        ));
  }
}

class ServiceDetailCard extends StatelessWidget {
  const ServiceDetailCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(offset: Offset(5, 5), blurRadius: 5, color: Colours.grey)
        ],
        color: Colors.white,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, right: 10),
            child: Image.asset(
              'assets/images/onboard1.PNG',
              width: 100,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Label('Window AC Fitting'),
                Text.rich(TextSpan(children: [
                  WidgetSpan(
                    child: Icon(
                      Icons.star,
                      color: Colours.green,
                      size: 14,
                    ),
                  ),
                  TextSpan(
                      text: '4.41', style: TextStyle(color: Colours.green)),
                  TextSpan(text: ' 3.7K ratings')
                ])),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text.rich(TextSpan(children: [
                      WidgetSpan(
                        child: Icon(
                          FontAwesome.inr,
                          size: 14,
                        ),
                      ),
                      TextSpan(text: '870')
                    ])),
                    Chip(
                      label: Text('Avail +'),
                      backgroundColor: Colours.orange,
                    )
                  ],
                ),
                Text('Price does not include spare part cost')
              ],
            ),
          )
        ],
      ),
    );
  }
}
