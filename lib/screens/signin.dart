import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool valid() {
    return true;
  }

  List otp_entered = ['-', '-', '-', '-'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colours.greybg,
        body: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/images/signin.PNG',
                      fit: BoxFit.fitWidth,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Label('Email Address'),
                          TextFormField(
                            decoration: InputStyle().login(),
                          ),
                          Label('Password'),
                          TextFormField(
                            decoration: InputStyle().login(),
                          ),
                          SizedBox(height: 20),
                          Center(child: Text('OR')),
                          SizedBox(height: 10),
                          Label('Enter PIN'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              OTPInput(autofocus: true, index: 0),
                              OTPInput(index: 1),
                              OTPInput(index: 2),
                              OTPInput(index: 3),
                            ],
                          ),
                          SizedBox(height: 10),
                          GestureDetector(
                            child: Center(
                              child: Text(
                                'Register for a new account',
                                style: TextStyle(color: Colours.blue),
                              ),
                            ),
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                  context, account_details);
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SubmitButton(onSubmit: () {
                // Navigator.pushNamed(context, new_account);
              })
            ],
          ),
        ));
  }

  Widget OTPInput({autofocus, index}) {
    return Container(
      width: 50.0,
      height: 50.0,
      margin: EdgeInsets.all(5.0),
      child: TextField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        maxLength: 1,
        onChanged: (value) {
          setState(() {
            otp_entered[index] = value;
          });
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
        decoration: InputDecoration(
          counterText: '',
        ),
      ),
    );
  }
}
