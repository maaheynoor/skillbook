import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';

class AccountDetails extends StatefulWidget {
  const AccountDetails({Key key}) : super(key: key);

  @override
  _AccountDetailsState createState() => _AccountDetailsState();
}

class _AccountDetailsState extends State<AccountDetails> {
  bool valid() {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colours.greybg,
        body: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/images/account_details.PNG',
                      fit: BoxFit.fitWidth,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              'Email Address',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          TextFormField(
                            decoration: InputStyle().login(),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: GestureDetector(
                              child: Text(
                                'Already have an account?',
                                style: TextStyle(color: Colours.blue),
                              ),
                              onTap: () {
                                Navigator.pushReplacementNamed(context, signin);
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              'Password',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          TextFormField(
                            decoration: InputStyle().login(),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SubmitButton(onSubmit: () {
                Navigator.pushNamed(context, new_account);
              })
            ],
          ),
        ));
  }
}
