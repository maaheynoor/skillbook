import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class NewMCQorAssignment extends StatefulWidget {
  const NewMCQorAssignment({Key key}) : super(key: key);

  @override
  _NewMCQorAssignmentState createState() => _NewMCQorAssignmentState();
}

class _NewMCQorAssignmentState extends State<NewMCQorAssignment> {
  String _type;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colours.pink,
        body: RoundedAppbar(
          title: Text(
            'Chapter 1',
            style: HeaderW1,
          ),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Label('Title'),
              Input(
                child: TextFormField(
                  decoration:
                      InputStyle(label: 'C++').input(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Label('Type'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Radio(
                        value: 'MCQ',
                        activeColor: Colors.black,
                        groupValue: _type,
                        onChanged: (value) {
                          setState(() {
                            _type = value;
                          });
                        },
                      ),
                      Text('MCQ'),
                    ],
                  ),
                  Row(
                    children: [
                      Radio(
                        value: 'Assignment',
                        activeColor: Colors.black,
                        groupValue: _type,
                        onChanged: (value) {
                          setState(() {
                            _type = value;
                          });
                        },
                      ),
                      Text('Assignment'),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Label('Deadline'),
              Input(
                child: TextFormField(
                  decoration: InputStyle(label: '').input(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Label('Instructions'),
              Input(
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  minLines: 5,
                  maxLines: 5,
                  decoration: InputStyle(label: '').input(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ]),
          ),
          submitBtn: SubmitButton(
              onSubmit: () {
                if (_type == 'MCQ') {
                  Navigator.pushNamed(context, add_mcq);
                } else if (_type == 'Assignment') {
                  Navigator.pushNamed(context, add_assignment);
                }
              },
              text: 'NEXT'),
        ));
  }
}
