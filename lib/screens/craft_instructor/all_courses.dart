import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/inputstyle.dart';

class AllCoursesPage extends StatefulWidget {
  const AllCoursesPage({this.student = false});
  final student;

  @override
  _AllCoursesPageState createState() => _AllCoursesPageState();
}

class _AllCoursesPageState extends State<AllCoursesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: TextFormField(
          decoration: InputStyle(label: 'Search').search(),
        ),
        actions: [
          CircleAvatar(
            radius: 15,
          )
        ],
      ),
      backgroundColor: Colours.orange,
      body: SafeArea(
        child: Theme(
          data: ThemeData(fontFamily: 'BebasNeue'),
          child: SingleChildScrollView(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                widget.student == true
                    ? Text(
                        'Continue',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Courses',
                            style: HeaderW2,
                          ),
                          Icon(
                            Icons.add,
                            size: 28,
                            color: Colors.white,
                          )
                        ],
                      ),
                for (var i = 0; i < 4; i++)
                  widget.student == true
                      ? CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          enroll_date: '11/05/21',
                          completed_percent: 30,
                          image: 'assets/images/python.png')
                      : CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
