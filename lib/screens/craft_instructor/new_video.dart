import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class AddNewVideo extends StatefulWidget {
  const AddNewVideo({Key key}) : super(key: key);

  @override
  _AddNewVideoState createState() => _AddNewVideoState();
}

class _AddNewVideoState extends State<AddNewVideo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                  Text(
                    'New Video',
                    style: HeaderB1,
                  ),
                  Label('Title'),
                  Input(
                    child: TextFormField(
                      decoration:
                          InputStyle(label: 'C++').input(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Label('Description'),
                  Input(
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      minLines: 2,
                      maxLines: 5,
                      decoration: InputStyle(
                              label:
                                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')
                          .input(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Label('Tags'),
                  Input(
                    child: TextFormField(
                      decoration: InputStyle(label: 'Programming, DSA')
                          .input(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Label('Upload'),
                      IconButton(icon: Icon(Icons.add), onPressed: () {})
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: [
                        TextFormField(
                          decoration: InputStyle(label: 'Chap 1')
                              .input(color: Colors.white),
                        ),
                        Divider(
                          height: 5,
                          thickness: 1,
                          color: Colours.grey,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('adfdfh.mp4'), Text('20:00')],
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colours.grey,
                            blurRadius: 5,
                            offset: const Offset(5, 5),
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            ),
            SubmitButton(onSubmit: () {}, text: 'NEXT')
          ],
        ),
      ),
    );
  }
}
