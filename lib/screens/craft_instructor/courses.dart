import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/drawer.dart';

class CoursesPage extends StatefulWidget {
  const CoursesPage({Key key}) : super(key: key);

  @override
  _CoursesPageState createState() => _CoursesPageState();
}

class _CoursesPageState extends State<CoursesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      drawer: DrawerWidget(),
      body: SafeArea(
        child: Theme(
          data: ThemeData(fontFamily: 'BebasNeue'),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: MediaQuery.of(context).size.height - 50,
                      width: MediaQuery.of(context).size.width - 10,
                      decoration: BoxDecoration(
                        color: Colours.green,
                        borderRadius:
                            BorderRadius.horizontal(right: Radius.circular(10)),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height - 40,
                      width: MediaQuery.of(context).size.width - 20,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colours.orange,
                        borderRadius:
                            BorderRadius.horizontal(right: Radius.circular(5)),
                      ),
                      child: Column(
                        children: [
                          AppBar(
                            backgroundColor: Colors.transparent,
                            elevation: 0,
                            title: Text(
                              'HOME',
                              style: TextStyle(color: Colors.white),
                            ),
                            actions: [
                              IconButton(
                                icon: Icon(Icons.search),
                                onPressed: () {},
                              ),
                              IconButton(
                                icon: Icon(Icons.chat_bubble_outline),
                                onPressed: () {},
                              ),
                              CircleAvatar(
                                radius: 15,
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colours.pink,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        'VIDEOS',
                                        style: HeaderB2,
                                      ),
                                      Text(
                                        '24',
                                        style: HeaderB1,
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 1,
                                  height: 60,
                                  color: Colors.black,
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        'VIEWS',
                                        style: HeaderB2,
                                      ),
                                      Text(
                                        '1,240',
                                        style: HeaderB1,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          CourseCard(
                              coursename: 'Python Programming',
                              instructor: 'Andrew Ng',
                              videos: 22,
                              students: 230,
                              image: 'assets/images/python.png'),
                          CourseCard(
                              coursename: 'Python Programming',
                              instructor: 'Andrew Ng',
                              videos: 22,
                              students: 230,
                              image: 'assets/images/python.png'),
                          IconButton(
                              icon: Icon(
                                FontAwesome5Solid.angle_double_down,
                                size: 20,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context, all_courses);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  'Browse',
                  style: HeaderB2,
                ),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                      ],
                    )),
                Text(
                  'Top Courses',
                  style: HeaderB2,
                ),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CourseCard(
                          coursename: 'Python Programming',
                          instructor: 'Andrew Ng',
                          videos: 22,
                          students: 230,
                          image: 'assets/images/python.png',
                          scroll: true,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
