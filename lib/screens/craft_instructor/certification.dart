import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class Certification extends StatefulWidget {
  const Certification({Key key}) : super(key: key);

  @override
  _CertificationState createState() => _CertificationState();
}

class _CertificationState extends State<Certification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                  Text(
                    'Certification',
                    style: HeaderB1,
                  ),
                  Label('Percentage to pass'),
                  Input(
                    child: TextFormField(
                      decoration:
                          InputStyle(label: '75%').input(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Label('Allot Examination'),
                  Input(
                      child: Container(
                    width: double.infinity,
                    color: Colors.white,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          child: Text('1. Chap 1'),
                          onTap: () {
                            Navigator.pushNamed(context, mcq_assignment);
                          },
                        ),
                        GestureDetector(
                          child: Text('2. Chap 2'),
                          onTap: () {
                            Navigator.pushNamed(context, mcq_assignment);
                          },
                        )
                      ],
                    ),
                  )),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            ),
            SubmitButton(onSubmit: () {}, text: 'NEXT')
          ],
        ),
      ),
    );
  }
}
