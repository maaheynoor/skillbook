import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class AddMCQ extends StatefulWidget {
  const AddMCQ({Key key}) : super(key: key);

  @override
  _AddMCQState createState() => _AddMCQState();
}

class _AddMCQState extends State<AddMCQ> {
  int questions = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      body: RoundedAppbar(
        title: Text(
          'Chapter 1\nAdd MCQ',
          style: HeaderW1,
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          for (var i = 1; i <= questions; i++) Question(),
          SizedBox(height: 20),
          Center(
            child: OutlinedButton(
              onPressed: () {
                setState(() {
                  questions += 1;
                });
              },
              child: Text(
                'Add New',
                style: TextStyle(color: Colors.black),
              ),
              style: OutlinedButton.styleFrom(
                  side: BorderSide(color: Colors.black),
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
          ),
          SizedBox(
            height: 60,
          ),
        ]),
        submitBtn: SubmitButton(
          onSubmit: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class Question extends StatefulWidget {
  const Question({Key key}) : super(key: key);

  @override
  _QuestionState createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  String _type;
  int options = 4, _correctOption, marks = 4;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Label('Question'),
        Input(
          child: TextFormField(
            maxLines: 2,
            decoration:
                InputStyle(label: 'Lorem Ipsum?').input(color: Colors.white),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Label('Option (Mark the correct one)'),
          SizedBox(
            width: 80,
            height: 48,
            child: PinkDropdown(
              list: [1, 2, 3, 4, 5, 6],
              value: options,
              color: Colors.white,
              onChanged: (op) {
                setState(() {
                  options = op;
                });
              },
            ),
          ),
        ]),
        SizedBox(
          height: 10,
        ),
        for (var i = 0; i < options; i++)
          Column(
            children: [
              Row(
                children: [
                  Radio(
                    value: i,
                    activeColor: Colors.black,
                    groupValue: _correctOption,
                    onChanged: (value) {
                      setState(() {
                        _correctOption = value;
                      });
                    },
                  ),
                  Expanded(
                    child: Input(
                        child: TextFormField(
                            decoration: InputStyle(label: '')
                                .input(color: Colors.white, borderRadius: 30)),
                        borderRadius: 30.0),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Label('Marks'),
          SizedBox(
            width: 80,
            height: 48,
            child: PinkDropdown(
              list: [1, 2, 3, 4, 5, 6],
              value: marks,
              color: Colors.white,
              onChanged: (mark) {
                setState(() {
                  marks = mark;
                });
              },
            ),
          ),
        ]),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
