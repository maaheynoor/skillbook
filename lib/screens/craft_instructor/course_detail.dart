import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/collapsibleCard.dart';

class CourseDetail extends StatefulWidget {
  const CourseDetail({Key key}) : super(key: key);

  @override
  _CourseDetailState createState() => _CourseDetailState();
}

class _CourseDetailState extends State<CourseDetail> {
  var course = {
    'name': 'Python Programming',
    'instructor': 'Andrew Ng',
    'videos': [
      {'no': 1, 'title': 'Classes', 'duration': '15:23'},
      {'no': 2, 'title': 'Polymorphism', 'duration': '13:33'},
      {'no': 3, 'title': 'Inheritence', 'duration': '15:23'},
      {'no': 4, 'title': 'Objects', 'duration': '1:23'}
    ],
    'assignments': [
      {'no': 1, 'title': 'Quiz 1', 'attempts': '15/23'},
      {'no': 2, 'title': 'Quiz 2', 'attempts': '13/33'},
    ],
    'students': 230,
    'duration': 3600,
    'image': 'assets/images/python.png'
  };

  @override
  Widget build(BuildContext context) {
    List videos = course["videos"];
    List assignments = course["assignments"];
    return Scaffold(
        backgroundColor: Colours.pink,
        body: SafeArea(
            child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Image.asset(
                    course['image'],
                    height: 40,
                    width: 40,
                    fit: BoxFit.contain,
                  ),
                  SizedBox(width: 5),
                  Expanded(
                    child: Text(
                      course['name'],
                      style: HeaderB2,
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      })
                ],
              ),
              Text(
                course['instructor'],
                style: HeaderB3,
              ),
              Text(
                '${videos.length} Videos in playlist',
                style: HeaderB3,
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RichText(
                      text: TextSpan(children: [
                    WidgetSpan(
                        child: Icon(
                      Icons.person,
                    )),
                    TextSpan(
                      text: '${course["students"]} Students enrolled',
                      style: HeaderB4,
                    )
                  ])),
                  Text(
                    '${course["duration"]}',
                    style: HeaderB4,
                  )
                ],
              ),
              CollapsibleCard(
                  title: 'Videos',
                  list: videos,
                  onAdd: () {
                    Navigator.pushNamed(context, craft_inst_new_video);
                  }),
              CollapsibleCard(
                  title: 'Assignments',
                  list: assignments,
                  onAdd: () {
                    Navigator.pushNamed(context, craft_inst_cert);
                  }),
            ],
          ),
        )));
  }
}
