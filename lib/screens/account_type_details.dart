import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';

class AccountTypeDetails extends StatefulWidget {
  const AccountTypeDetails({this.type});
  final String type;

  @override
  _AccountTypeDetailsState createState() => _AccountTypeDetailsState();
}

class _AccountTypeDetailsState extends State<AccountTypeDetails> {
  bool valid() {
    return true;
  }

  List otp_entered = ['-', '-', '-', '-'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colours.greybg,
        body: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/images/account_type_details.PNG',
                      fit: BoxFit.fitWidth,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Label('Username'),
                          TextFormField(
                            decoration: InputStyle().login(),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Label('Choose PIN'),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              'Note: This PIN can be used to login to your account',
                              style: TextStyle(color: Colours.blue),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              OTPInput(autofocus: true, index: 0),
                              OTPInput(index: 1),
                              OTPInput(index: 2),
                              OTPInput(index: 3),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SubmitButton(onSubmit: () {
                Navigator.pushNamed(context, user_type_details,
                    arguments: widget.type);
              })
            ],
          ),
        ));
  }

  Widget OTPInput({autofocus, index}) {
    return Container(
      width: 50.0,
      height: 50.0,
      margin: EdgeInsets.all(5.0),
      child: TextField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        maxLength: 1,
        onChanged: (value) {
          setState(() {
            otp_entered[index] = value;
          });
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
        decoration: InputDecoration(
          counterText: '',
        ),
      ),
    );
  }
}
