import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:flutter/gestures.dart';
import 'package:skillbook/utils/text_styles.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  var data = [
    {
      "title": "INNOVATED BY",
      "detail":
          "DR. KIRAN PRAKASH ZARKAR\nAuthor, Innovator, Craft Instructor (COPA)\nBrand Ambassator",
      "image": "assets/images/onboard2.PNG"
    },
    {
      "title":
          "FOR FARMERS-PEOPLES, TRAINEE - CRAFT INSTRUCTOR - ITI, STUDENTS - TEACHERS - SCHOOL, INSTITUTES, COLLEGES, COMPANIES, STARTUP, SELP HELP GROUPS",
      "detail":
          "Find courses,jobs, apprentices, services, startups, entrepreneurship all in one place",
      "image": "assets/images/onboard3.PNG"
    },
  ];
  final PageController _controller = PageController(
    initialPage: 0,
    keepPage: false,
  );

  void onNavigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isonboarding", false);
    Navigator.pushNamedAndRemoveUntil(
        context, account_details, (route) => false);
  }

  Widget _onBoarding(int index) {
    return Column(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: 50,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colours.orange,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(10))),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: 2,
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  flex: 5,
                                  child: Text(
                                    data[index]['title'],
                                    style: HeaderW2,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: 2,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 20),
                          // index == 0
                          //     ? Column(
                          //         children: [
                          //           Padding(
                          //             padding: const EdgeInsets.symmetric(
                          //                 horizontal: 10),
                          //             child: Text(
                          //               data[index]['detail'],
                          //               textAlign: TextAlign.center,
                          //               style: HeaderW2,
                          //             ),
                          //           ),
                          //           SizedBox(height: 10),
                          //           Image.asset(data[index]['image'],
                          //               width: double.infinity,
                          //               fit: index != 1
                          //                   ? BoxFit.fitWidth
                          //                   : BoxFit.contain),
                          //         ],
                          //       )
                          //     :
                          Column(
                            children: [
                              Image.asset(data[index]['image'],
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height / 4,
                                  fit: index != 0
                                      ? BoxFit.fitWidth
                                      : BoxFit.contain),
                              SizedBox(height: 10),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  data[index]['detail'],
                                  textAlign: TextAlign.center,
                                  style: HeaderW3,
                                ),
                              ),
                            ],
                          ),
                          index == 1
                              ? TextButton(
                                  onPressed: () {
                                    onNavigate();
                                  },
                                  child: Text(
                                    'GET STARTED',
                                    style: HeaderW3,
                                  ),
                                  style: TextButton.styleFrom(
                                      backgroundColor: Colours.green,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20))),
                                )
                              : Container(),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ]);
  }

  int index = 0;

  _onPageViewChange(int page) {
    setState(() {
      index = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity, //MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              PageView(
                physics: AlwaysScrollableScrollPhysics(),
                reverse: false,
                scrollDirection: Axis.horizontal,
                controller: _controller,
                pageSnapping: true,
                onPageChanged: _onPageViewChange,
                children: <Widget>[
                  _onBoarding(0),
                  _onBoarding(1),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      for (var i = 0; i < 2; i++)
                        Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: i == index ? 6 : 4,
                            )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
