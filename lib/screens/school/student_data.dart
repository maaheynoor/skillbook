import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class StudentData extends StatefulWidget {
  const StudentData({Key key}) : super(key: key);

  @override
  _StudentDataState createState() => _StudentDataState();
}

class _StudentDataState extends State<StudentData> {
  var standard, course, institute;
  var std_list = ['X', 'XI', 'XII', 'Under Grad Under Grad', 'Grad'];
  var course_list = ['Btech', 'B.E.', 'B.A.', 'B.Sc.'];
  var inst_list = ['Vidyalay', 'School', 'COE'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: [
          CircleAvatar(
            radius: 20,
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              'Student Database',
              style: HeaderB1,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  WhiteDropdown(
                      list: std_list,
                      value: standard,
                      onChanged: (val) {
                        setState(() {
                          standard = val;
                        });
                      },
                      hint: 'Standard'),
                  SizedBox(width: 10),
                  WhiteDropdown(
                      list: course_list,
                      value: course,
                      onChanged: (val) {
                        setState(() {
                          course = val;
                        });
                      },
                      hint: 'Course'),
                  SizedBox(width: 10),
                  WhiteDropdown(
                      list: inst_list,
                      value: institute,
                      onChanged: (val) {
                        setState(() {
                          institute = val;
                        });
                      },
                      hint: 'Institute'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(5),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text('Aditya Sharma', style: TextStyle(fontSize: 12)),
                          SizedBox(
                            width: 10,
                          ),
                          Text('adsharma@gmail.com',
                              style: TextStyle(fontSize: 12))
                        ],
                      ),
                      IconButton(
                        icon: Icon(FontAwesome.ellipsis_h),
                        onPressed: () {
                          Navigator.pushNamed(context, studentdetail);
                        },
                      ),
                    ],
                  ),
                  Text('Not Verified', style: TextStyle(fontSize: 8)),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text('Aditya Sharma', style: TextStyle(fontSize: 12)),
                          SizedBox(
                            width: 10,
                          ),
                          Text('adsharma@gmail.com',
                              style: TextStyle(fontSize: 12))
                        ],
                      ),
                      IconButton(
                        icon: Icon(FontAwesome.ellipsis_h),
                        onPressed: () {
                          Navigator.pushNamed(context, studentdetail);
                        },
                      ),
                    ],
                  ),
                  Text('Verified', style: TextStyle(fontSize: 8)),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
