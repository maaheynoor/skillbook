import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class StudentDetail extends StatefulWidget {
  const StudentDetail({Key key}) : super(key: key);

  @override
  _StudentDetailState createState() => _StudentDetailState();
}

class _StudentDetailState extends State<StudentDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: [
          CircleAvatar(
            radius: 20,
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              'Student Database',
              style: HeaderB1,
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(20),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 40,
                      ),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Label(
                            'ADITYA SHARMA\nSkillman\nGold Account',
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  Label('Contact Details'),
                  Input(
                      borderRadius: 10.0,
                      child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colours.lightpink),
                          padding: EdgeInsets.all(10),
                          child: Label(
                              'Phone - 9xxxxxxxxx \nEmail - adasharma@gmail.com'))),
                  Label('Qualification'),
                  Input(
                      borderRadius: 10.0,
                      child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colours.lightpink),
                          padding: EdgeInsets.all(10),
                          child: Label(
                              'Diploma Course \nHTYR University \t\t 2016-17'))),
                  Label('Current Address'),
                  Input(
                      borderRadius: 10.0,
                      child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colours.lightpink),
                          padding: EdgeInsets.all(10),
                          child: Label('Lorem Ipsun defijikm'))),
                  Label('Pincode'),
                  Input(
                      borderRadius: 10.0,
                      child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colours.lightpink),
                          padding: EdgeInsets.all(10),
                          child: Label('4000192'))),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
