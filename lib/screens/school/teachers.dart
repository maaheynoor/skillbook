import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';

class Teachers extends StatefulWidget {
  const Teachers({Key key}) : super(key: key);

  @override
  _TeachersState createState() => _TeachersState();
}

class _TeachersState extends State<Teachers> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  String _type;
  List list = [
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh'
  ];
  var minDisplay;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    minDisplay = 10 < list.length ? 10 : list.length;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colours.pink,
      drawer: DrawerWidget(),
      body: RoundedAppbar(
        appbarColor: Colours.pink,
        backgroundColor: Colours.orange,
        bottomBGColor: Colours.pink,
        title: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.menu, color: Colors.black),
            onPressed: () {
              scaffoldKey.currentState.openDrawer();
            },
          ),
          title: Text(
            'HOME',
            style: HeaderB4,
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.search, color: Colors.black),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.chat_bubble_outline, color: Colors.black),
              onPressed: () {},
            ),
            CircleAvatar(
              radius: 15,
            )
          ],
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Teachers',
            style: HeaderW1,
          ),
          for (var i = 0; i < minDisplay; i++)
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${i + 1}. ${list[i]}",
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                  Icon(Icons.visibility, color: Colors.white),
                ],
              ),
            ),
          list.length > 10
              ? Center(
                  child: IconButton(
                      icon: Icon(
                          minDisplay == list.length
                              ? FontAwesome5Solid.angle_double_up
                              : FontAwesome5Solid.angle_double_down,
                          size: 20,
                          color: Colors.white),
                      onPressed: () {
                        setState(() {
                          if (minDisplay == list.length) {
                            minDisplay = 10;
                          } else {
                            minDisplay = list.length;
                          }
                        });
                      }),
                )
              : Container(),
        ]),
        bottomWidget: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Top Courses',
              style: HeaderB2,
            ),
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CourseCard(
                      coursename: 'Python Programming',
                      instructor: 'Andrew Ng',
                      videos: 22,
                      students: 230,
                      image: 'assets/images/python.png',
                      scroll: true,
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
