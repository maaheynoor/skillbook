import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/RoundedAppbar.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/widgets/courseCard.dart';

class Webinar extends StatefulWidget {
  const Webinar({Key key}) : super(key: key);

  @override
  _WebinarState createState() => _WebinarState();
}

class _WebinarState extends State<Webinar> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  String _type;
  List list = [
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh',
    'Ramesh'
  ];
  var minDisplay;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    minDisplay = 10 < list.length ? 10 : list.length;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colours.orange,
      body: RoundedAppbar(
        appbarColor: Colours.pink,
        backgroundColor: Colours.orange,
        title: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'Webinar',
            style: HeaderB2,
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.search, color: Colors.black),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.chat_bubble_outline, color: Colors.black),
              onPressed: () {},
            ),
            CircleAvatar(
              radius: 15,
            )
          ],
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Current',
            style: HeaderW2,
          ),
          WebinarCard(
              name: 'Resume Building Webinar',
              instructor: 'Dr. Abhay Kapoor',
              institute: 'BITS Mesra',
              date: '01/06/21',
              time: '9:00 Am')
        ]),
      ),
    );
  }
}

class WebinarCard extends StatelessWidget {
  WebinarCard({
    this.name,
    this.instructor,
    this.institute,
    this.date,
    this.time,
  });
  final name, instructor, institute, date, time;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colours.pink,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
            ),
            Text(instructor),
            Text(institute),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(TextSpan(children: [
                  WidgetSpan(child: Icon(Icons.calendar_today)),
                  TextSpan(text: date)
                ])),
                Text.rich(TextSpan(children: [
                  WidgetSpan(child: Icon(Icons.lock_clock)),
                  TextSpan(text: time)
                ]))
              ],
            )
          ],
        ));
  }
}
