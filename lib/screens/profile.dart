import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.pink,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: [
              Icon(Icons.arrow_back, color: Colors.black),
              Text('BACK', style: TextStyle(color: Colors.black))
            ],
          ),
        ),
        leadingWidth: double.maxFinite,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: 50,
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Aditya Sharma',
                      style: HeaderB2,
                    ),
                    Text('BIT Mesra Btech Student\nNot Verified\nFree Account')
                  ],
                )
              ],
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  ListTile(
                    title: Text('Videos Watched'),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colours.grey,
                    ),
                  ),
                  ListTile(
                    title: Text('Resume'),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colours.grey,
                    ),
                  ),
                  ListTile(
                    title: Text('Job/Apprenticeship Application'),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colours.grey,
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, myjobs);
                    },
                  ),
                  ListTile(
                    title: Text('Upgrade Account'),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colours.grey,
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, upgrade_account);
                    },
                  ),
                  ListTile(
                    title: Text('Certificates'),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colours.grey,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
