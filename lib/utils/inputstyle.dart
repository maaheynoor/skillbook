import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';

class InputStyle {
  String label;
  int errLines;
  bool date;
  InputStyle({label, errLines, date}) {
    this.label = label;
    this.errLines = errLines;
    this.date = date;
  }
  login() {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      filled: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30.0),
        borderSide: BorderSide.none,
      ),
      errorStyle: TextStyle(
        color: Colours.grey,
      ),
      errorMaxLines: errLines ?? 1,
    );
  }

  input({Color color, double borderRadius}) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      filled: true,
      fillColor: color,
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: borderRadius != null
            ? BorderRadius.circular(borderRadius)
            : BorderRadius.circular(0),
      ),
      hintText: label,
      hintStyle: TextStyle(
        color: label == 'Browse' ? Colours.blue : Colors.black38,
      ),
      errorStyle: TextStyle(
        color: Colours.grey,
      ),
    );
  }

  search() {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      filled: true,
      fillColor: Colors.white,
      focusColor: Colors.black,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30.0),
        // borderSide: BorderSide.none,
      ),
      hintText: label,
      prefixIcon: Icon(Icons.search),
      suffixIcon: Icon(FontAwesome.filter),
      errorStyle: TextStyle(
        color: Colours.grey,
      ),
      errorMaxLines: errLines ?? 1,
    );
  }

  datepicker() {
    return InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        filled: true,
        fillColor: Colours.pink,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
          borderSide: BorderSide.none,
        ),
        hintText: 'Enter',
        hintStyle: TextStyle(
          color: Colors.black38,
          fontSize: 16,
        ),
        errorStyle: TextStyle(
          color: Colors.red,
        ),
        suffixIcon: IconButton(
          icon: date != null && date
              ? Icon(Icons.calendar_today_outlined)
              : Icon(Icons.keyboard_arrow_down),
          color: Colors.black38,
          onPressed: () {},
        ));
  }
}
