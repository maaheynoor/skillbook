import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';

const HeaderW1 =
    TextStyle(color: Colors.white, fontSize: 56, fontFamily: 'SquadaOne');
const HeaderW2 =
    TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'SquadaOne');
const HeaderW3 =
    TextStyle(color: Colors.white, fontSize: 18, fontFamily: 'SquadaOne');
const HeaderW4 = TextStyle(color: Colors.white, fontFamily: 'SquadaOne');
const HeaderB1 =
    TextStyle(color: Colors.black, fontSize: 48, fontFamily: 'SquadaOne');
const HeaderB2 =
    TextStyle(color: Colors.black, fontSize: 32, fontFamily: 'SquadaOne');
const HeaderB3 =
    TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'SquadaOne');
const HeaderB4 = TextStyle(color: Colors.black, fontFamily: 'SquadaOne');
const HeaderG3 =
    TextStyle(color: Colours.green, fontSize: 18, fontFamily: 'SquadaOne');
