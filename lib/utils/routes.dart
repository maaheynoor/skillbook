import 'package:flutter/material.dart';
import 'package:skillbook/screens/account_details.dart';
import 'package:skillbook/screens/company/job_applications.dart';
import 'package:skillbook/screens/company/new_job.dart';
import 'package:skillbook/screens/company/open_positions.dart';
import 'package:skillbook/screens/new_account.dart';
import 'package:skillbook/screens/account_type_details.dart';
import 'package:skillbook/screens/school/student_data.dart';
import 'package:skillbook/screens/school/student_detail.dart';
import 'package:skillbook/screens/school/webinar.dart';
import 'package:skillbook/screens/settings.dart';
import 'package:skillbook/screens/skillman/services.dart';
import 'package:skillbook/screens/student/jobs.dart';
import 'package:skillbook/screens/student/my_jobs.dart';
import 'package:skillbook/screens/student/reel.dart';
import 'package:skillbook/screens/student/services.dart';
import 'package:skillbook/screens/student/skill_dev.dart';
import 'package:skillbook/screens/student/skill_groups.dart';
import 'package:skillbook/screens/user_type_details.dart';
import 'package:skillbook/screens/signin.dart';
import 'package:skillbook/screens/craft_instructor/courses.dart';
import 'package:skillbook/screens/craft_instructor/course_detail.dart';
import 'package:skillbook/screens/craft_instructor/new_video.dart';
import 'package:skillbook/screens/craft_instructor/mcq_assignment.dart';
import 'package:skillbook/screens/craft_instructor/add_mcq.dart';
import 'package:skillbook/screens/craft_instructor/add_assignment.dart';
import 'package:skillbook/screens/craft_instructor/all_courses.dart';
import 'package:skillbook/screens/craft_instructor/certification.dart';
import 'package:skillbook/screens/school/teachers.dart';
import 'package:skillbook/screens/student/dashboard.dart';
import 'package:skillbook/screens/profile.dart';
import 'package:skillbook/screens/student/upgrade_account.dart';
import 'package:skillbook/screens/student/messages.dart';
import 'package:skillbook/screens/student/search_skillman.dart';

const String account_details = '/account_details';
const String new_account = '/new_account';
const String account_type_details = '/account_type_details';
const String user_type_details = '/user_type_details';
const String signin = '/signin';
const String craft_inst_courses = '/craft_inst_courses';
const String craft_inst_course_detail = '/craft_inst_course_detail';
const String craft_inst_new_video = '/craft_inst_new_video';
const String craft_inst_cert = '/craft_inst_cert';
const String mcq_assignment = '/mcq_assignment';
const String add_mcq = '/add_mcq';
const String add_assignment = '/add_assignment';
const String all_courses = '/all_courses';
const String inst_teachers = '/inst_teachers';
const String open_positions = '/open_positions';
const String new_job = '/new_job';
const String job_applications = '/job_applications';
const String student_dashboard = '/student_dashboard';
const String profile = '/profile';
const String upgrade_account = '/upgrade_account';
const String messages = '/messages';
const String services = '/services';
const String searchskillman = '/searchskillman';
const String webinar = '/webinar';
const String studentdata = '/studentdata';
const String studentdetail = '/studentdetail';
const String skillman_services = '/skillman_services';
const String settingspage = '/settings';
const String myjobs = '/myjobs';
const String jobs = '/jobs';
const String skill_dev = '/skilldev';
const String skill_groups = '/skill_groups';
const String reels = '/reels';

class CustomRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case account_details:
        return MaterialPageRoute(
            settings: settings, builder: (_) => AccountDetails());
      case new_account:
        return MaterialPageRoute(
            settings: settings, builder: (_) => NewAccount());
      case account_type_details:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => AccountTypeDetails(type: settings.arguments));
      case user_type_details:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => UserTypeDetails(type: settings.arguments));
      case signin:
        return MaterialPageRoute(settings: settings, builder: (_) => SignIn());
      case craft_inst_courses:
        return MaterialPageRoute(
            settings: settings, builder: (_) => CoursesPage());
      case craft_inst_course_detail:
        return MaterialPageRoute(
            settings: settings, builder: (_) => CourseDetail());
      case craft_inst_new_video:
        return MaterialPageRoute(
            settings: settings, builder: (_) => AddNewVideo());
      case craft_inst_cert:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Certification());
      case mcq_assignment:
        return MaterialPageRoute(
            settings: settings, builder: (_) => NewMCQorAssignment());
      case add_mcq:
        return MaterialPageRoute(settings: settings, builder: (_) => AddMCQ());
      case add_assignment:
        return MaterialPageRoute(
            settings: settings, builder: (_) => AddAssignment());
      case all_courses:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => AllCoursesPage(
                  student: settings.arguments,
                ));
      case inst_teachers:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Teachers());
      case open_positions:
        return MaterialPageRoute(
            settings: settings, builder: (_) => OpenPositions());
      case new_job:
        return MaterialPageRoute(settings: settings, builder: (_) => NewJob());
      case job_applications:
        return MaterialPageRoute(
            settings: settings, builder: (_) => JobApplications());
      case student_dashboard:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Dashboard());
      case profile:
        return MaterialPageRoute(settings: settings, builder: (_) => Profile());
      case upgrade_account:
        return MaterialPageRoute(
            settings: settings, builder: (_) => UpgradeAccount());
      case messages:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Messages());
      case services:
        return MaterialPageRoute(
            settings: settings, builder: (_) => ServicesPage());
      case searchskillman:
        return MaterialPageRoute(
            settings: settings, builder: (_) => SearchSkillMan());
      case webinar:
        return MaterialPageRoute(settings: settings, builder: (_) => Webinar());
      case studentdata:
        return MaterialPageRoute(
            settings: settings, builder: (_) => StudentData());
      case studentdetail:
        return MaterialPageRoute(
            settings: settings, builder: (_) => StudentDetail());
      case skillman_services:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Services());
      case settingspage:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Settings());
      case myjobs:
        return MaterialPageRoute(settings: settings, builder: (_) => MyJobs());
      case jobs:
        return MaterialPageRoute(settings: settings, builder: (_) => Jobs());
      case skill_dev:
        return MaterialPageRoute(
            settings: settings, builder: (_) => SkillDevelop());
      case skill_groups:
        return MaterialPageRoute(
            settings: settings, builder: (_) => SkillGroups());
      case reels:
        return MaterialPageRoute(
            settings: settings, builder: (_) => ReelPage());
    }
  }
}
