import 'package:flutter/material.dart';

class Colours {
  static const blue = Color(0xff5a95ee);
  static const darkgreen = Color(0xff1D2E0E);
  static const grey = Color(0xffc2c8ce);
  static const greybg = Color(0xffefefef);
  static const pink = Color(0xffFFE2E2);
  static const lightpink = Color(0xfffcecec);
  static const darkpink = Color(0xfffb7c7c);
  static const orange = Color(0xffEF7F1B);
  static const green = Color(0xff5eb611);
}
