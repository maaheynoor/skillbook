import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';

class CourseCard extends StatelessWidget {
  CourseCard(
      {this.coursename,
      this.instructor,
      this.videos,
      this.students,
      this.image,
      this.enroll_date,
      this.completed_percent,
      this.scroll = false});
  final coursename,
      instructor,
      videos,
      students,
      image,
      enroll_date,
      completed_percent,
      scroll;
  @override
  Widget build(BuildContext context) {
    return scroll
        ? Container(
            margin: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width / 2,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(2, 0),
                      blurRadius: 10,
                      spreadRadius: 1,
                      color: Colors.grey)
                ]),
            child: Column(
              children: [
                Image.asset(
                  image,
                  fit: BoxFit.contain,
                ),
                Text(
                  coursename,
                  style: HeaderB2,
                ),
                Text(
                  instructor,
                  style: HeaderB3,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, craft_inst_course_detail);
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colours.pink,
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(20)),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      'View Course',
                      style: HeaderB4,
                    ),
                  ),
                ),
              ],
            ),
          )
        : GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, craft_inst_course_detail);
            },
            child: Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colours.pink,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          image,
                          height: 40,
                          width: 40,
                          fit: BoxFit.contain,
                        ),
                        SizedBox(width: 5),
                        Expanded(
                          child: Text(
                            coursename,
                            style: HeaderB2,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      instructor,
                      style: HeaderB3,
                    ),
                    enroll_date != null
                        ? Text(
                            enroll_date,
                            style: HeaderB3,
                          )
                        : Container(),
                    videos != null
                        ? Text(
                            '${videos} Videos in playlist',
                            style: HeaderB3,
                          )
                        : Container(),
                    SizedBox(height: 5),
                    students != null
                        ? Row(
                            children: [
                              Icon(Icons.person),
                              Text(
                                '${students} Students enrolled',
                                style: HeaderB4,
                              ),
                            ],
                          )
                        : Container(),
                    completed_percent != null
                        ? ProgressBar(percent: completed_percent)
                        : Container()
                  ],
                )),
          );
  }
}

class ProgressBar extends StatelessWidget {
  const ProgressBar({this.percent});
  final percent;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        Container(
          width: 250,
          height: 15,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10),
              color: Colors.white),
        ),
        Container(
            width: 250 * percent / 100,
            height: 13,
            decoration: BoxDecoration(
              color: percent >= 50 ? Colours.green : Colours.darkpink,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            )),
      ],
    );
  }
}
