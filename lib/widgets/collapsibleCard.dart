import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'dart:math';

class CollapsibleCard extends StatefulWidget {
  CollapsibleCard({this.title, this.list, this.onAdd});
  final title, list, onAdd;

  @override
  _CollapsibleCardState createState() => _CollapsibleCardState();
}

class _CollapsibleCardState extends State<CollapsibleCard> {
  var minDisplay;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    minDisplay = 3 < widget.list.length ? 3 : widget.list.length;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.title,
                  style: HeaderB2,
                ),
                IconButton(
                    icon: Icon(Icons.add, size: 30), onPressed: widget.onAdd)
              ],
            ),
            for (var i = 0; i < minDisplay; i++)
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${widget.list[i]['no']}. ${widget.list[i]['title']}",
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      widget.list[i]['duration'] != null
                          ? "${widget.list[i]['duration']}"
                          : "${widget.list[i]['attempts']}",
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
            widget.list.length > 3
                ? IconButton(
                    icon: Icon(
                        minDisplay == widget.list.length
                            ? FontAwesome5Solid.angle_double_up
                            : FontAwesome5Solid.angle_double_down,
                        size: 20),
                    onPressed: () {
                      setState(() {
                        if (minDisplay == widget.list.length) {
                          minDisplay = 3;
                        } else {
                          minDisplay = widget.list.length;
                        }
                      });
                    })
                : Container(),
          ],
        ));
  }
}
