import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/text_styles.dart';

class SubmitButton extends StatelessWidget {
  SubmitButton({this.onSubmit, this.text});
  final onSubmit, text;

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.bottomCenter,
        child: GestureDetector(
            onTap: onSubmit,
            child: Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              color: Colours.darkgreen,
              child: Text(
                text ?? 'SUBMIT',
                style: HeaderW3,
              ),
            )));
  }
}

class Label extends StatelessWidget {
  Label(this.label);
  final label;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Text(
        label,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }
}

class Input extends StatelessWidget {
  Input({this.child, this.borderRadius});
  final child, borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: child,
      decoration: BoxDecoration(
        borderRadius: borderRadius != null
            ? BorderRadius.circular(borderRadius)
            : BorderRadius.circular(0),
        boxShadow: [
          BoxShadow(
            color: Colours.grey,
            blurRadius: 5,
            offset: const Offset(5, 5),
          ),
        ],
      ),
    );
  }
}

class PinkDropdown extends StatelessWidget {
  PinkDropdown({this.list, this.value, this.onChanged, this.color, this.hint});

  final List list;
  var value;
  final onChanged, color, hint;

  @override
  Widget build(BuildContext context) {
    return Input(
        child: InputDecorator(
      decoration:
          InputStyle(label: "Enter").input(color: color ?? Colours.pink),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          dropdownColor: color ?? Colours.pink,
          value: value,
          isExpanded: true,
          //elevation: 5,
          style: TextStyle(color: Colors.white),
          iconEnabledColor: Colors.black38,
          icon: Icon(Icons.keyboard_arrow_down),
          hint: Text(
            hint ?? 'Enter',
            style: TextStyle(color: Colors.black38, fontSize: 16),
          ),
          items: list.map<DropdownMenuItem>((value) {
            return DropdownMenuItem(
              value: value,
              child: Text(
                value.toString(),
                style: TextStyle(color: Colors.black),
              ),
            );
          }).toList(),

          onChanged: (value) {
            this.value = value;
            this.onChanged(value);
          },
        ),
      ),
    ));
  }
}

class WhiteDropdown extends StatelessWidget {
  WhiteDropdown({this.list, this.value, this.onChanged, this.color, this.hint});

  final List list;
  var value;
  final onChanged, color, hint;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      width: 120,
      child: InputDecorator(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(5),
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            dropdownColor: Colors.white,
            value: value,
            isExpanded: true,
            //elevation: 5,
            style: TextStyle(color: Colors.white),
            iconEnabledColor: Colors.black38,
            icon: Icon(Icons.keyboard_arrow_down),
            hint: Text(
              hint ?? 'Enter',
              style: TextStyle(color: Colors.black38, fontSize: 16),
            ),
            items: list.map<DropdownMenuItem>((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(
                  value.toString(),
                  style: TextStyle(color: Colors.black),
                ),
              );
            }).toList(),

            onChanged: (value) {
              this.value = value;
              this.onChanged(value);
            },
          ),
        ),
      ),
    );
  }
}
