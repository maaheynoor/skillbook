import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/inputstyle.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/widgets/drawer.dart';
import 'package:skillbook/widgets/form_fields.dart';
import 'package:skillbook/utils/text_styles.dart';

class RoundedAppbar extends StatelessWidget {
  const RoundedAppbar(
      {this.title,
      this.child,
      this.bottomWidget,
      this.submitBtn,
      this.appbarColor,
      this.backgroundColor,
      this.bottomBGColor});
  final title,
      child,
      bottomWidget,
      submitBtn,
      appbarColor,
      backgroundColor,
      bottomBGColor;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 40),
                    width: double.infinity,
                    color: appbarColor ?? Colours.orange,
                    child: title),
                Transform.translate(
                  offset: Offset(0.0, -30),
                  child: Container(
                      padding: EdgeInsets.all(10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: backgroundColor ?? Colours.pink,
                          borderRadius: BorderRadius.circular(20)),
                      child: child),
                ),
                bottomWidget ?? Container()
              ],
            ),
          ),
          submitBtn ?? Container()
        ],
      ),
    );
  }
}
