import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:flutter_icons/flutter_icons.dart';

class StackedScreen extends StatelessWidget {
  const StackedScreen({this.mainWidgets, this.onViewMore, this.bottomWidgets});
  final mainWidgets, onViewMore, bottomWidgets;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Theme(
        data: ThemeData(fontFamily: 'BebasNeue'),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: MediaQuery.of(context).size.height - 150,
                    width: MediaQuery.of(context).size.width - 10,
                    decoration: BoxDecoration(
                      color: Colours.green,
                      borderRadius:
                          BorderRadius.horizontal(right: Radius.circular(10)),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height - 140,
                    width: MediaQuery.of(context).size.width - 20,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colours.orange,
                      borderRadius:
                          BorderRadius.horizontal(right: Radius.circular(5)),
                    ),
                    child: Column(
                      children: [
                        AppBar(
                          backgroundColor: Colors.transparent,
                          elevation: 0,
                          leading: IconButton(
                            icon: Icon(Icons.menu),
                            onPressed: () {
                              Scaffold.of(context).openDrawer();
                            },
                          ),
                          title: Text(
                            'HOME',
                            style: TextStyle(color: Colors.white),
                          ),
                          actions: [
                            IconButton(
                              icon: Icon(Icons.search),
                              onPressed: () {},
                            ),
                            IconButton(
                              icon: Icon(Icons.chat_bubble_outline),
                              onPressed: () {
                                Navigator.pushNamed(context, messages);
                              },
                            ),
                            CircleAvatar(
                              radius: 15,
                            )
                          ],
                        ),
                        mainWidgets,
                        IconButton(
                            icon: Icon(
                              FontAwesome5Solid.angle_double_down,
                              size: 20,
                              color: Colors.white,
                            ),
                            onPressed: onViewMore),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              bottomWidgets
            ],
          ),
        ),
      ),
    );
  }
}
