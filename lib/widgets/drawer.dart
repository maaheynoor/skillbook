import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/courseCard.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/screens/student/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({Key key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  String accountType;
  var listActions;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPrefs();
  }

  void getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      accountType = prefs.getString("account_type");
      if (accountType == 'Crafts Instructor' ||
          accountType == 'Teacher/Professor') {
        listActions = {
          'Teachers': () {
            Navigator.pop(context);
            Navigator.pushNamed(context, inst_teachers);
          },
          'Students Enrolled': () {
            Navigator.pop(context);
            Navigator.pushNamed(context, studentdata);
          },
          'Assignments': null,
          'Research Projects': null,
          'Webinar': () {
            Navigator.pop(context);
            Navigator.pushNamed(context, webinar);
          },
        };
      } else {
        listActions = {
          'My Courses': () {
            Navigator.pop(context);
            Navigator.pushNamed(context, all_courses, arguments: true);
          },
          'Favourites': null,
          'Jobs/ Services/ Skill Development': () {
            Navigator.pop(context);
            Navigator.pushNamed(context, jobs);
          },
          'Assignments': null,
          'Subscriptions': null,
          'Document Submissions': null
        };
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.horizontal(right: Radius.circular(5)),
      child: Drawer(
        child: Container(
          color: Colours.orange,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  GestureDetector(
                      child: SafeArea(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colours.orange,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, profile);
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CircleAvatar(
                                  radius: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Aditya Sharma', style: HeaderW3),
                                    Text(
                                      'Not Verified Account |\n $accountType',
                                      style: TextStyle(color: Colors.white),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                                Icon(Icons.chevron_right, color: Colors.white)
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {}),
                  for (var k in listActions.keys)
                    CenterListTile(
                      text: k,
                      onTap: listActions[k] ??
                          () {
                            Navigator.pop(context);
                          },
                    ),
                ],
              ),
              Column(
                children: [
                  CenterListTile(
                    text: 'Settings',
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, settingspage);
                    },
                    left: true,
                  ),
                  CenterListTile(
                    text: 'Help',
                    onTap: () {
                      Navigator.pop(context);
                    },
                    left: true,
                  ),
                  CenterListTile(
                    text: 'Logout',
                    onTap: () {
                      Navigator.pop(context);
                    },
                    left: true,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CenterListTile extends StatelessWidget {
  CenterListTile({this.text, this.onTap, this.left});
  final text, onTap, left;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: left == true ? EdgeInsets.all(0) : EdgeInsets.only(left: 20),
      child: ListTile(
          title: Text(
            text,
            style: TextStyle(color: Colors.white),
          ),
          onTap: onTap),
    );
  }
}
