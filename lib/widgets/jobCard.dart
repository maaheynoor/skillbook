import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/routes.dart';
import 'package:skillbook/utils/text_styles.dart';
import 'package:skillbook/widgets/form_fields.dart';

class JobCard extends StatelessWidget {
  const JobCard(
      {this.jobtitle, this.company, this.location, this.ctc, this.views});
  final jobtitle, company, location, ctc, views;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, job_applications);
      },
      child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colours.orange,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(jobtitle, style: HeaderW2),
                  Text.rich(
                      TextSpan(children: [
                        WidgetSpan(
                            child: Icon(FontAwesome.inr,
                                color: Colors.white, size: 14)),
                        TextSpan(text: ctc)
                      ]),
                      style: HeaderW4)
                ],
              ),
              Text(company, style: HeaderW2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    location,
                    style: HeaderW4,
                  ),
                  Text.rich(
                      TextSpan(children: [
                        WidgetSpan(
                            child: Icon(Icons.visibility,
                                color: Colors.white, size: 14)),
                        TextSpan(text: views.toString())
                      ]),
                      style: HeaderW4)
                ],
              ),
            ],
          )),
    );
  }
}

class WhiteJobCard extends StatelessWidget {
  WhiteJobCard(
      {this.image,
      this.role,
      this.company,
      this.jobtype,
      this.applicants,
      this.postdate});
  final image, role, company, jobtype, applicants, postdate;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Input(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                ListTile(
                    leading: Image.asset(image),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          role,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(company),
                        Text(jobtype),
                        Text('$applicants Applicants')
                      ],
                    ),
                    trailing: Icon(Icons.chevron_right)),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Text('$postdate days ago'),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }
}
