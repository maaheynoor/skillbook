import 'package:flutter/material.dart';
import 'package:skillbook/utils/colours.dart';
import 'package:skillbook/utils/text_styles.dart';

class CustomTab extends StatelessWidget {
  CustomTab({this.title, this.number, this.active, this.onTap});
  final title, number, active, onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width / 3 - 10,
        height: MediaQuery.of(context).size.width / 3 - 10,
        decoration: BoxDecoration(
          color: Colours.lightpink,
          borderRadius: active
              ? BorderRadius.vertical(top: Radius.circular(20))
              : BorderRadius.circular(20),
          border: active
              ? Border.all(width: 0, color: Colours.lightpink)
              : Border.all(color: Colors.black),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: HeaderB3,
              textAlign: TextAlign.center,
            ),
            number != null
                ? Text(
                    number.toString(),
                    style: HeaderB1,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
